import React, { Component, PropTypes } from 'react';
import {
	StyleSheet,
	View,
	Text,
	Image,
} from 'react-native';

import logoImg from '../images/Layer141.png';
import First from '../images/layer_.png';
import Second from '../images/layer_2.png';



export default class Logo extends Component {
	render() {
		return (
			<View style={styles.container}>
			<Image source={First} style={styles.image1}/>
			<Image source={Second} style={styles.image2}/>
				<Image source={logoImg} style={styles.image} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 3,
		alignItems: 'center',
		justifyContent: 'center',
	},
	image: {
		width: 200,
		height: 200,
		top:15
	},
	image1: {
		width: '60%',
		height: '10%',
		bottom:10,
	},
	image2: {
		width: '60%',
		height: '10%',
		bottom:10,
		top:5,
	},
	text: {
		color: '#727272',
		fontWeight: 'bold',
		backgroundColor: 'transparent',
		marginTop: 20,
		fontSize:20
	},
	text_2: {
		color: '#727272',
		fontWeight: 'normal',
		backgroundColor: 'transparent',
		marginTop: 5,
		fontSize:15
	}
});
