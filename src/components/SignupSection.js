import React, { Component, PropTypes } from 'react';
import Dimensions from 'Dimensions';
import {
	StyleSheet,
	View,
	Text,
	Image
} from 'react-native';


import passwordImg from '../images/layer_3.png';
export default class SignupSection extends Component {
	render() {
		return (
			<View style={styles.container}>
                <Image source={passwordImg} style={styles.image} />
			</View>
		);
	}
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		bottom: 40,
		width: DEVICE_WIDTH - 40,
		flexDirection: 'row',
		alignItems: 'center',
		paddingLeft: 45,
	},
	text: {
		color: '#727272',
		backgroundColor: 'transparent',
	},
	image: {
		width: DEVICE_WIDTH,
		height: 10,
		flex: 1,
	},
});
